package net.nilosplace.acuitas;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.nilosplace.acuitas.model.BackTestResults;
import si.mazi.rescu.ClientConfig;
import si.mazi.rescu.RestProxyFactory;
import si.mazi.rescu.serialization.jackson.JacksonObjectMapperFactory;

public class AcuitasMain {

	public static void main(String[] args) throws Exception {
		
		
		ClientConfig config = new ClientConfig();
		
		JacksonObjectMapperFactory json = new JacksonObjectMapperFactory() {
			
			@Override
			public ObjectMapper createObjectMapper() { return new ObjectMapper(); }
			
			@Override
			public void configureObjectMapper(ObjectMapper objectMapper) {
				
			}
		};
		
		config.setJacksonObjectMapperFactory(json);
		
		
		AcuitasCoreAPIInterface api = RestProxyFactory.createProxy(AcuitasCoreAPIInterface.class, "http://10.10.10.1:2112", config);
	
			
		//BufferedReader in = new BufferedReader(new FileReader("pairs_list2")); 
		
		//String text;
		//while((text = in.readLine()) != null) {
			//BackTestResults result = api.createBBStrategy(text, 75, 75);
			//System.out.println(text);
			//System.out.println(result.getUuid());
			//api.setStrategy(result.getUuid());
		//}
		//in.close();
		

//		for(int i = 0; i <= 195; i+=15) {
//			for(int j = 165; j <= 195; j+=15) {
//				BackTestResults result = api.createBBStrategy("ETC/BTC", i, j, new BigDecimal("100"));
//				System.out.println(i + "," + j + " " + result.getTotal_pct_gain());
//			}
//		}
		

		for(int i = 2; i <= 15; i+=1) {
			for(int j = i+1; j <= 40; j+=1) {
				BackTestResults result = api.createEMASpread(
						"ETH/BTC",
						"limit",
						new BigDecimal(0.0500),
						"percent",
						new BigDecimal(5.00),
						new BigDecimal(0.07266175),
						"fok",
						"limit",
						"fok",
						new BigDecimal(0.2), 
						"300",
						"168h",
						i,
						j,
						new BigDecimal(0.2),
						new BigDecimal(0.1),
						new BigDecimal(1),
						new BigDecimal(0),
						new BigDecimal(0),
						1545786480539L,
						1545181680539L,
						-1,
						0,
						"down");
				
				System.out.println(i + "," + j + "," + result.getTotal_pct_gain());
			}
		}

		
		
//		for(int i = 1; i <= 15; i++) {
//			for(int j = i + 1; j <= 15; j++) {
//				BackTestResults result = api.createEMAStrategy("ETC/BTC", i, j, new BigDecimal(1.000));
//				System.out.println(i + "," + j + " " + result.getTotal_pct_gain());
//			}
//		}
	

	}

}
