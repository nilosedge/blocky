package net.nilosplace.acuitas;

import java.math.BigDecimal;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import net.nilosplace.acuitas.model.BackTestResults;
import net.nilosplace.acuitas.model.PaperTradeResult;

@Path("")
public interface AcuitasCoreAPIInterface {

	@POST
	@Path("/backtesting/strategies/ema_spread")
	public BackTestResults createEMASpread(
		@FormParam("pair") String pair,
		@FormParam("orderBuyType") String orderBuyType,
		@FormParam("startingCapital") BigDecimal startingCapital,
		
		@FormParam("orderBuyAmountType") String orderBuyAmountType,
		@FormParam("orderBuyAmountTypePercent") BigDecimal orderBuyAmountTypePercent,
		@FormParam("orderBuyAmountTypeFixed") BigDecimal orderBuyAmountTypeFixed,		
		@FormParam("orderBuyTypeValue") String orderBuyTypeValue,
		
		@FormParam("orderSellType") String orderSellType,
		@FormParam("orderSellTypeValue") String orderSellTypeValue,
		
		
		@FormParam("slippageFeesPercent") BigDecimal slippageFeesPercent,
		@FormParam("period") String period,
		@FormParam("timespan") String timespan,
		@FormParam("emaFast") Integer emaFast,
		@FormParam("emaSlow") Integer emaSlow,
		
		
		@FormParam("buySpread") BigDecimal buySpread,
		@FormParam("sellSpread") BigDecimal sellSpread,
		
		@FormParam("stopLossPercent") BigDecimal stopLossPercent,
		@FormParam("totalTrailingPercentBuy") BigDecimal totalTrailingPercentBuy,
		@FormParam("totalTrailingPercentSell") BigDecimal totalTrailingPercentSell,

		@FormParam("endTime") Long endTime,
		@FormParam("startTime") Long startTime,
		@FormParam("bearGuardStrength") Integer bearGuardStrength,
		@FormParam("rsiLength") Integer rsiLength,
		@FormParam("crossType") String crossType
	);

	
	@POST
	@Path("/backtesting/strategies/ema_ema")
	public BackTestResults createEMA(
		@FormParam("pair") String pair,
		@FormParam("ema1Timeframe") Integer ema1Timeframe,
		@FormParam("startingCapital") BigDecimal startingCapital,
		@FormParam("ema2Timeframe") Integer ema2Timeframe,
		@FormParam("period") String period,
		@FormParam("gainPercent") BigDecimal gainPercent,
		@FormParam("timespan") String timespan,
		@FormParam("endTime") Long endTime,
		@FormParam("startTime") Long startTime
	);

	@POST
	@Path("/papertrading/from-bt/{uuid}")
	public PaperTradeResult setStrategy(
		@PathParam("uuid") String uuid
	);
	
}
