package net.nilosplace.acuitas;

import java.awt.Dimension;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.LongSummaryStatistics;
import java.util.stream.DoubleStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ShowHeatMap extends JPanel {

	JFrame frame = new JFrame("DrawRect");


	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		try {
			File f = new File("heat_map2");
			BufferedReader reader = new BufferedReader(new FileReader(f));
			String line;
			
			ArrayList<String[]> arrayList = new ArrayList<>();
			ArrayList<Double> doubles = new ArrayList<>();

			while((line = reader.readLine()) != null) {
				String[] arr = line.split(",");
				doubles.add(Double.parseDouble(arr[2]));
				arrayList.add(arr);
			}

			
			
			
			DoubleSummaryStatistics stats = doubles.stream().collect(DoubleSummaryStatistics::new, DoubleSummaryStatistics::accept, DoubleSummaryStatistics::combine);
			System.out.println("Max:"+stats.getMax()+", Min:"+stats.getMin());
			System.out.println("Count:"+stats.getCount()+", Sum:"+stats.getSum());
			System.out.println("Average:"+stats.getAverage());
			
			
			for(String[] array: arrayList) {
				//System.out.println(line);
				//g.setColor(Color);
				g.fillRect(Integer.parseInt(array[0]), Integer.parseInt(array[1]), 15, 15);
			}
			
			
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Dimension getPreferredSize() {
		// so that our GUI is big enough
		return new Dimension(200, 200);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}

	public static void createAndShowGui() {

		ShowHeatMap mainPanel = new ShowHeatMap();

		JFrame frame = new JFrame("DrawRect");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
}
