package net.nilosplace.acuitas.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class FullTrade {

	//private BigDecimal tradeProfit;
	private BigDecimal p_l;
	private Trade entry;
	private Trade exit;

}
