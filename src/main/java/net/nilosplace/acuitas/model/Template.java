package net.nilosplace.acuitas.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Template {

	private String highBandSell;
	private String period;
	private String smaTimeFrame;
	private String startTime;
	private String timespan;
	private String endTime;
	private String lowBandBuy;
	private String startingCapital;
	private String pair;
	private String standardDeviation;
	
	private BigDecimal stopLossPercent;
	private BigDecimal ema2Timeframe;
	private BigDecimal ema1Timeframe;
	private BigDecimal gainPercent;


}

