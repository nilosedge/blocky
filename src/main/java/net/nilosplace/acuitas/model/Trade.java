package net.nilosplace.acuitas.model;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class Trade {

	private Date date;
	private String price;
	private Long timestamp;
	private BigDecimal quantity_placed;
	private String index;

}
