package net.nilosplace.acuitas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class TimeStamp {

	private String date;
	private Long timestamp;

}
