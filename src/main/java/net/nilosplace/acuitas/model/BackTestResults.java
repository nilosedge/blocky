package net.nilosplace.acuitas.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BackTestResults {

	private BigDecimal ending_capital;
	private Template template;
	private Integer period;
	private BigDecimal risk_reward_ratio;
	private List<Strategy> strategies;
	private List<FullTrade> trades;
	private String uuid;
	private String pair;
	private BigDecimal buy_and_hold;
	private BigDecimal total_txn_cost;
	private Integer number_of_trades;
	private Series series;
	private BigDecimal total_pct_gain;
	private BigDecimal starting_capital;
	private BigDecimal max_drawdown;
	private Strategy strategy;
	private BigDecimal total_p_l;
	private Date last_order_close_time;

	
}
