package net.nilosplace.acuitas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class Series {

	private TimeStamp start;
	private TimeStamp end;

}
