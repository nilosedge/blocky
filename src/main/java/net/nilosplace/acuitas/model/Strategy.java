package net.nilosplace.acuitas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class Strategy {

	private String name;
	private String description;
	private String rule_details;
	private String key;
	private Template template;
	
}
