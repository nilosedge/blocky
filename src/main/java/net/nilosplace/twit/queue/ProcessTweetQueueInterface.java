package net.nilosplace.twit.queue;

public interface ProcessTweetQueueInterface {
	public void processTweet(String json);
}
