package net.nilosplace.twit.queue;

import net.openhft.chronicle.queue.service.ServiceWrapper;
import net.openhft.chronicle.queue.service.ServiceWrapperBuilder;

public class MainTwitAnalyzer {

	public static void main(String[] args) {

		ServiceWrapper serviceWrapper = ServiceWrapperBuilder.serviceBuilder(
				"TweetCollection", 
				"TweetSentiment", 
				ProcessTweetQueueInterface.class, 
				TwitterTweetAnalyzer::new).get();
	}

}
