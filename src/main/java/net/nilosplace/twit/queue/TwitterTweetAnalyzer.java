package net.nilosplace.twit.queue;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.ejml.simple.SimpleMatrix;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twitter.twittertext.Extractor;
import com.vader.sentiment.analyzer.SentimentAnalyzer;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import net.nilosplace.twit.model.TwitterTweet;
import net.nilosplace.twit.util.EmojiUtils;

public class TwitterTweetAnalyzer implements TwitterTweetQueueInterface {

	private ObjectMapper mapper = new ObjectMapper();
	private ProcessTweetQueueInterface listener;
	private StanfordCoreNLP pipeline;
	private int count = 0;

	public TwitterTweetAnalyzer(ProcessTweetQueueInterface listener) {
		this.listener = listener;
		Properties properties = new Properties();
		//annotators = tokenize, ssplit, parse, sentiment
		properties.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
		pipeline = new StanfordCoreNLP(properties);
	}

	@Override
	public void saveTweet(String json) {
		TwitterTweet mess;
		try {
			mess = mapper.readValue(json, TwitterTweet.class);
			//System.out.println(mess.getLang());
			if(!"en".equals(mess.getLang())) {
				// Non english
			} else if(mess.getRetweeted_status() != null) {
				// Retweet
			} else if(mess.getQuoted_status() != null) {
				// Quoted someone else
			} else if(mess.getTruncated() && mess.getExtended_tweet() != null) {
				// Extended tweet
			} else {
				//Extractor extractor = new Extractor();
				//TwitterTextParseResults result = TwitterTextParser.parseTweet("");

				System.out.println(json);
				System.out.println("C: " + count + " L: " + mess.getText().length() + " S: " + getSentiment(mess.getText()) + " M: " + mess.getText());
				System.out.println("PR: " + SentimentAnalyzer.getScoresFor(mess.getText()));
				
				//String json = mapper.writeValueAsString(mess);
				//System.out.println(count + ": " + json);
			}
			//mapper.enable(SerializationFeature.INDENT_OUTPUT);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * "Very negative" = 0
	 * "Negative" = 1
	 * "Neutral" = 2
	 * "Positive" = 3
	 * "Very positive" = 4
	 */
	public int getSentiment(String text) {

		Extractor extractor = new Extractor();

		text = text.replaceAll("\\n", " ");
		text = text.replaceAll("-", " ");
		text = text.replaceAll("\\+", " ");

		List<String> cashes = extractor.extractCashtags(text);
		List<String> hashes = extractor.extractHashtags(text);
		List<String> names  = extractor.extractMentionedScreennames(text);
		List<String> urls = extractor.extractURLs(text);

		text = EmojiUtils.removeEmoji(text);

		for(String c: cashes) text = text.replace("$" + c, "");
		for(String h: hashes) text = text.replace("#" + h, "");
		for(String n: names) text = text.replace("@" + n, "");
		for(String u: urls) text = text.replace(u, "");

		text = text.replaceAll("(\\.|,|\\?|'|!)+", "");
		text = text.replaceAll("…", "");
		text = text.replaceAll("  ", " ");
		text = text.trim();

		System.out.println("New Text: " + text);

		if (text != null && text.length() > 0) {

			Annotation annotation = pipeline.process(text);

			for(CoreMap sentence: annotation.get(CoreAnnotations.SentencesAnnotation.class)) {

				System.out.println("Sentence: " + sentence);
				Tree tree = sentence.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);

				SimpleMatrix simpleMatrix = RNNCoreAnnotations.getPredictions(tree);

				System.out.println("Matrix: " + simpleMatrix);
				//classification.setVeryNegative((int)Math.round(simpleMatrix.get(0)*100d));
				//classification.setNegative((int)Math.round(simpleMatrix.get(1)*100d));
				//classification.setNeutral((int)Math.round(simpleMatrix.get(2)*100d));
				//classification.setPositive((int)Math.round(simpleMatrix.get(3)*100d));
				//classification.setVeryPositive((int)Math.round(simpleMatrix.get(4)*100d));
				String setimentType = sentence.get(SentimentCoreAnnotations.SentimentClass.class);

				System.out.println("S: " + setimentType);
				//sentimentResult.setSentimentType(setimentType);
				//sentimentResult.setSentimentClass(classification);
				System.out.println("Sc: " + RNNCoreAnnotations.getPredictedClass(tree));
				//sentimentResult.setSentimentScore(RNNCoreAnnotations.getPredictedClass(tree));

			}

		}

		return 0;

	}



}
