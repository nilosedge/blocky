package net.nilosplace.twit.queue;

import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import net.openhft.chronicle.queue.ChronicleQueue;
import net.openhft.chronicle.queue.impl.single.SingleChronicleQueueBuilder;

public class MainTwitCollector {

	public static void main(String[] args) throws Exception {
		ChronicleQueue output = SingleChronicleQueueBuilder.binary("TweetCollection").build();

		TwitterTweetQueueInterface writer = output.acquireAppender().methodWriter(TwitterTweetQueueInterface.class);

		BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);
		BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<Event>(1000);

		Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
		StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
		// Optional: set up some followings and track terms
		//List<Long> followings = Lists.newArrayList(1234L, 566788L);
		List<String> terms = Lists.newArrayList(
				"bitcoin", "btc",
				"ripple", "xrp",
				"ethereum", "eth",
				"bitcoin cash", "bch",
				"eos", "dash",
				"stellar", "xlm",
				"tether", "usdt",
				"litecoin", "ltc",
				"bitcoin sv", "bsv",
				"tron", "trx",
				"cardano", "ada",
				"iota", "miota",
				"binance coin", "bnb",
				"monero", "xmr",
				"crypto", "cryptocurrency");
		//hosebirdEndpoint.followings(followings);
		hosebirdEndpoint.trackTerms(terms);

		// These secrets should be read from a config file
		Authentication hosebirdAuth = new OAuth1(
				"7UWA7yjEDayj829rBrjnhzYjQ",
				"mlCr0avHG7c7yO6n3zyYcTwGHnXigdEV1EfT9wEemGboqHj995",
				"18002257-3jfeADXKSpqYTvJWqNNkPkiaWxHOxHvZdMYqyA7VT",
				"4gWA4UxDI5c16SIyeRpdZ7ViE6wX5dCak3u8uBVmcOJhE"
				);

		ClientBuilder builder = new ClientBuilder()
				.name("Hosebird-Client-01")                              // optional: mainly for the logs
				.hosts(hosebirdHosts)
				.authentication(hosebirdAuth)
				.endpoint(hosebirdEndpoint)
				.processor(new StringDelimitedProcessor(msgQueue))
				.eventMessageQueue(eventQueue);                          // optional: use this if you want to process client events

		Client hosebirdClient = builder.build();
		// Attempts to establish a connection.
		hosebirdClient.connect();
		Date start = new Date();
		int count = 0;
		while (!hosebirdClient.isDone()) {
			String msg = msgQueue.take();
			writer.saveTweet(msg);
			Date end = new Date();
			long time = end.getTime() - start.getTime();
			if(time > 60000) {
				System.out.println("Count: " + count + " Time: " + end);
				count = 0;
				start = new Date();
			}
			count++;
		}

	}

}
