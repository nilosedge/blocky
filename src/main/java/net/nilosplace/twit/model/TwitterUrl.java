package net.nilosplace.twit.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterUrl extends TwitterId {
	String url;
	String expanded;
	String expanded_url;
	String display;
	String display_url;
	List<Integer> indices;
}
