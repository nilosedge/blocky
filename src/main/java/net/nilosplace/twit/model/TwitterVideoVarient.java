package net.nilosplace.twit.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterVideoVarient {
	Integer bitrate;
	String content_type;
	String url;
}
