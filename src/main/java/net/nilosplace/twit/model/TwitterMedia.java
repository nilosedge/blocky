package net.nilosplace.twit.model;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterMedia extends TwitterId {

	List<Integer> indices;
	String description;
	TwitterMediaInfo additional_media_info;
	String media_url;
	String media_url_https;
	String url;
	String display_url;
	String expanded_url;
	String type;
	TwitterVideoInfo video_info;
	HashMap<String, TwitterVideoSize> sizes;
	
	Long source_status_id;
	String source_status_id_str;
	Long source_user_id;
	String source_user_id_str;
	
}
