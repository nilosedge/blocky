package net.nilosplace.twit.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TwitterLimit {
	Integer track;
	Date timestamp_ms;
}
