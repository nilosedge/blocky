package net.nilosplace.twit.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterSymbol {
	String text;
	List<Integer> indices;
}
