package net.nilosplace.twit.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TwitterScope {
	Boolean followers;
}
