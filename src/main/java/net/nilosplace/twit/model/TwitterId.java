package net.nilosplace.twit.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterId {
	// Sun Dec 30 05:25:03 +0000 2018
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE MMM dd hh:mm:ss zzz yyyy")
	Date created_at;
	Long id;
	String id_str;
}
