package net.nilosplace.twit.model;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterTweet extends TwitterId {

	String full_text;
	String text;
	List<Integer> display_text_range;
	String source;
	Boolean truncated;
	Long in_reply_to_status_id;
	String in_reply_to_status_id_str;
	Long in_reply_to_user_id;
	String in_reply_to_user_id_str;
	String in_reply_to_screen_name;
	TwitterUser user;
	TwitterCoordinate geo;
	TwitterCoordinate coordinates;
	TwitterPlace place;
	String contributors;
	List<String> withheld_in_countries;
	Long quoted_status_id;
	String quoted_status_id_str;
	TwitterTweet quoted_status;
	TwitterTweet retweeted_status;
	TwitterTweet extended_tweet;
	TwitterUrl quoted_status_permalink;
	Boolean is_quote_status;
	Integer quote_count;
	Integer reply_count;
	Integer retweet_count;
	Integer favorite_count;
	TwitterEntity entities;

	HashMap<String, List<TwitterMedia>> extended_entities;
	Boolean favorited;
	Boolean retweeted;
	Boolean possibly_sensitive;
	TwitterScope scopes;
	String filter_level;
	String lang;
	
	TwitterLimit limit;
	Date timestamp_ms;
	
}
