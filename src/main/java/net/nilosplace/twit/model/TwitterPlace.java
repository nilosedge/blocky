package net.nilosplace.twit.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterPlace {

	String id;
	String url;
	String place_type;
	String name;
	String full_name;
	String country_code;
	String country;
	
	TwitterBoundingBox bounding_box;
	TwitterAttribute attributes;
	
}
