package net.nilosplace.twit.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterUserMention extends TwitterId {
	String screen_name;
	String name;
	List<Integer> indices;
}
