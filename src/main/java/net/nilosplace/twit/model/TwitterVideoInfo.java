package net.nilosplace.twit.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterVideoInfo {
	List<Integer> aspect_ratio;
	Integer duration_millis;
	List<TwitterVideoVarient> variants;
}
