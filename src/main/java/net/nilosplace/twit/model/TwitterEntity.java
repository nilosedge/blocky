package net.nilosplace.twit.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterEntity {

	List<TwitterHashtag> hashtags;
	List<TwitterUrl> urls;
	List<TwitterUserMention> user_mentions;

	List<TwitterMedia> media;
	List<TwitterSymbol> symbols;
	//"polls":[]

}
