package net.nilosplace.twit.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterMediaInfo {
	String title;
	String description;
	Boolean embeddable;
	Boolean monetizable;
}
