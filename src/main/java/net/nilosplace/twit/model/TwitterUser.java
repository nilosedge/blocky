package net.nilosplace.twit.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwitterUser extends TwitterId {

	String name;
	String screen_name;
	String location;
	String url;
	String description;
	String translator_type;
	@JsonProperty("protected")
	Boolean protectedValue;
	Boolean verified;
	Integer followers_count;
	Integer friends_count;
	Integer listed_count;
	Integer favourites_count;
	Integer statuses_count;
	String utc_offset;
	String time_zone;
	Boolean geo_enabled;
	String lang;
	Boolean contributors_enabled;
	String profile_background_color;
	Boolean is_translator;
	String profile_background_image_url;
	String profile_background_image_url_https;
	Boolean profile_background_tile;
	String profile_link_color;
	String profile_sidebar_border_color;
	String profile_sidebar_fill_color;
	String profile_text_color;
	Boolean profile_use_background_image;
	String profile_image_url;
	String profile_image_url_https;
	String profile_banner_url;
	Boolean default_profile;
	Boolean default_profile_image;
	Boolean following;
	Boolean follow_request_sent;
	Boolean notifications;
	
	
}
