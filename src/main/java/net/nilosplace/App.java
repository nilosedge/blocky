package net.nilosplace;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.Block;
import org.bitcoinj.core.Context;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionInput;
import org.bitcoinj.core.TransactionOutput;
import org.bitcoinj.core.Utils;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.script.Script;
import org.bitcoinj.utils.BlockFileLoader;

public class App {

	static PrintWriter blockWriter = null;
	static PrintWriter blockToBlockWriter = null;
	static PrintWriter transactionWriter = null;
	static PrintWriter blockTransactionWriter = null;
	static PrintWriter coinbaseWriter = null;

	static PrintWriter inTransactionWriter = null;
	static PrintWriter outTransactionWriter = null;
	static PrintWriter outAddressWriter = null;

	public App() throws Exception {
		NetworkParameters np = new MainNetParams();
		List<File> blockChainFiles = new ArrayList<>();
		Context.propagate(new Context(np));

		File directory = new File("/Users/oblod/Library/Application Support/Bitcoin/blocks/");
		FileFilter filter = new WildcardFileFilter("blk*");
		File[] files = directory.listFiles(filter);
		Date lastTime = null;
		Arrays.sort(files);

		boolean flush = true;

		blockWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/blocks")), flush);
		blockToBlockWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/blockBlocks")), flush);
		transactionWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/trans")), flush);
		blockTransactionWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/blockTrans")), flush);
		coinbaseWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/coinbase")), flush);

		inTransactionWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/inTrans")), flush);
		outTransactionWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/outTrans")), flush);
		outAddressWriter = new PrintWriter(new FileOutputStream(new File("/Users/oblod/import/outAddress")), flush);

		for(File f: files) {
			blockChainFiles.add(f);
		}

		BlockFileLoader bfl = new BlockFileLoader(np, blockChainFiles);

		boolean first = true;
		boolean blockFound = false;

		for (Block block : bfl) {

//			if(!block.getHashAsString().equals("0000000000000083429a0549717670e55a76ccc51d824cbd573e644a5fc0664c") && !blockFound) {
//				continue;
//			} else {
//				blockFound = true;
//			}

			if(first) {
				blockWriter.println(block.getPrevBlockHash());
				first = false;
			}

			blockWriter.println(block.getHashAsString());
			blockToBlockWriter.println(block.getPrevBlockHash() + "," + block.getHashAsString());

			Date now = new Date();
			if(lastTime == null || now.getTime() - lastTime.getTime() > 5000) {
				System.out.println("Block: " + block.getPrevBlockHash() + " -> " + block.getHashAsString() + " " + block.getTime());
				lastTime = now;
			}

			boolean transFound = false;
			for(Transaction t: block.getTransactions()) {

				//if(!block.getHashAsString().equals("514c46f0b61714092f15c8dfcb576c9f79b3f959989b98de3944b19d98832b58") && !transFound) {
				//	continue;
				//} else {
				///	transFound = true;
				//}


				transactionWriter.println(t.getHashAsString());


				blockTransactionWriter.println(block.getHashAsString() + "," + t.getHashAsString());

				for(TransactionInput in: t.getInputs()) {
					if(t.isCoinBase()) {
						coinbaseWriter.println(t.getHashAsString());
					} else {
						inTransactionWriter.println(t.getHashAsString() + "," + in.getOutpoint().getHash() + "," + in.getOutpoint().getIndex());
					}
				}

				int index = 0;
				for(TransactionOutput out: t.getOutputs()) {

					Script script = out.getScriptPubKey();
					List<String> addresses = new ArrayList<String>();
					if(script.isSentToAddress() || script.isPayToScriptHash()) {
						Address a = script.getToAddress(np);
						addresses.add(a.toString());
					} else if(script.isSentToRawPubKey()) {
						Address a = new Address(np, Utils.sha256hash160(script.getPubKey()));
						addresses.add(a.toString());
					} else if(script.isSentToMultiSig()) {
						List<ECKey> keyList = script.getPubKeys();
						for(ECKey key: keyList) {
							System.out.println("MultiSig: " + Utils.HEX.encode(key.getPubKeyHash()));
							addresses.add(Utils.HEX.encode(key.getPubKeyHash()));
						}
						//System.out.println("Multi Sig: ");
						//System.out.println(t);
						//System.out.println(out);
						//System.out.println(addresses);
						//System.exit(-1);
					} else {
						System.out.println(t);
						System.out.println(out);
						//System.out.println(script.getScriptType());
//						List<ECKey> keyList = script.getPubKeys();
//						for(ECKey key: keyList) {
//							System.out.println("Non MultiSig: " + Utils.HEX.encode(key.getPubKeyHash()));
//							addresses.add(Utils.HEX.encode(key.getPubKeyHash()));
//						}
						//System.exit(-1);
					}

					for(String address: addresses) {
						outAddressWriter.println(address + "," + t.getHashAsString() + "," + index);
					}
					outTransactionWriter.println(t.getHashAsString() + "," + index + "," + out.getValue());
					index++;
				}

			}
		}
	}

	private static void closeFiles() {
		blockWriter.close();
		blockTransactionWriter.close();
		coinbaseWriter.close();
		inTransactionWriter.close();
		outTransactionWriter.close();
		outAddressWriter.close();
		blockToBlockWriter.close();
		transactionWriter.close();
	}

	public static void main( String[] args ) {
		try {
			new App();
			closeFiles();
		} catch (Exception e) {
			e.printStackTrace();
			closeFiles();
		}	
	}
}
