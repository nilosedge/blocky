package net.nilosplace.bitcoin;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import si.mazi.rescu.ClientConfig;
import si.mazi.rescu.RestProxyFactory;
import si.mazi.rescu.serialization.jackson.JacksonObjectMapperFactory;

public class BitCoinCoreAPI {

	private BitCoinCoreAPIInterface api;

	public BitCoinCoreAPI() {
		ClientConfig config = new ClientConfig();
		
		JacksonObjectMapperFactory json = new JacksonObjectMapperFactory() {
			
			@Override
			public ObjectMapper createObjectMapper() { return new ObjectMapper(); }
			
			@Override
			public void configureObjectMapper(ObjectMapper objectMapper) {
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			}
		};
		
		config.setJacksonObjectMapperFactory(json);
		
		
		api = RestProxyFactory.createProxy(BitCoinCoreAPIInterface.class, "http://localhost:2112", config);
	}
	
	public Block getBlock(String id) {
		return api.getBlock(id);
	}
	
	
}
