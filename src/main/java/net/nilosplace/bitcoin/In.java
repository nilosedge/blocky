package net.nilosplace.bitcoin;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class In {

	private String coinbase;
	private Long sequence;
	
	private String txid;
	@JsonProperty("vout")
	private Integer vout_index;
	
	private ScriptSig scriptSig;
	private String asm;
	private String hex;

}
