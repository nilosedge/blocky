package net.nilosplace.bitcoin;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class ScriptSig {

	private String asm;
	private String hex;
}
