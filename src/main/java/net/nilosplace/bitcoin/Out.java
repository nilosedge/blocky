package net.nilosplace.bitcoin;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class Out {
	
	private BigDecimal value;
	@JsonProperty("n")
	private Integer index;
	private Script scriptPubKey;

}
