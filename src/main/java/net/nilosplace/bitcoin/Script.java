package net.nilosplace.bitcoin;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class Script {

	private String asm;
	private String hex;
	private Integer reqSigs;
	private String type;
	private List<String> addresses;
}
