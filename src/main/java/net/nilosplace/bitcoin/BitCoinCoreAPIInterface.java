package net.nilosplace.bitcoin;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/rest")
public interface BitCoinCoreAPIInterface {

	@GET
	@Path("/block/{id}.json")
	public Block getBlock(
		@PathParam("id") String id
	);
}
