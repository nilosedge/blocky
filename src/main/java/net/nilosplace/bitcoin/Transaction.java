package net.nilosplace.bitcoin;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter @Setter @ToString
public class Transaction {
	
	private String txid;
	private String hash;
	private Integer version;
	private Integer size;
	private Integer vsize;
	private Integer locktime;
	
	private List<In> vin;
	private List<Out> vout;
	
	private String hex;
	
}
