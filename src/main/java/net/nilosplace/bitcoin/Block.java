package net.nilosplace.bitcoin;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@JsonIgnoreProperties(ignoreUnknown = false)
public class Block {

	private String hash;
	private Integer confirmations;
	private Integer strippedsize;
	private Integer size;
	private Integer weight;
	private Integer height;
	private Integer version;
	private String versionHex;
	private String merkleroot;
	private List<Transaction> tx;
	
	private Date time;
	private Date mediantime;
	private Long nonce;
	private String bits;
	private Integer difficulty;
	private String chainwork;
	@JsonProperty("nTx")
	private Integer nTx;
	private String previousblockhash;
	private String nextblockhash;

}
